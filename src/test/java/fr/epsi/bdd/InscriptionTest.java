package fr.epsi.bdd;

import fr.epsi.beans.Fcsv;
import fr.epsi.beans.User;
import java.sql.SQLException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class InscriptionTest {

    public InscriptionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of conex method, of class Inscription.
     *
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    @Test
    public void testConexFalse() throws SQLException, ClassNotFoundException {
        Inscription inscriptionTest = new Inscription();
        Assert.assertFalse(inscriptionTest.conex("email", "password"));
    }

    /**
     * Test of conex method, of class Inscription.
     *
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    @Test
    public void testConexTrue() throws SQLException, ClassNotFoundException {
        Inscription inscriptionTest = new Inscription();
        Assert.assertTrue(inscriptionTest.conex("admin.admin@gmail.com", "admin"));
    }

    /**
     * Test of addcsv method, of class Inscription.
     *
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    @Test
    public void testAddcsv() throws SQLException, ClassNotFoundException {
        Inscription instance = new Inscription();
        Fcsv fcsvTest = new Fcsv(100, "testLibelle", "testValeur");
        instance.addcsv(fcsvTest);
        Assert.assertEquals(100, fcsvTest.getIdFiche());
        Assert.assertEquals("testLibelle", fcsvTest.getLibelle());
        Assert.assertEquals("testValeur", fcsvTest.getValeur());
    }

    /**
     * Test of addUser method, of class Inscription.
     *
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    @Test
    public void testAddUser() throws SQLException, ClassNotFoundException {
        Inscription instance = new Inscription();
        User userTest = new User(100, "testNom", "testPrenom", "testSolution", "testEmail", "testPassword");
        instance.addUser(userTest);
        Assert.assertEquals(100, userTest.getIdUser());
        Assert.assertEquals("testNom", userTest.getNom());
        Assert.assertEquals("testPrenom", userTest.getPrenom());
        Assert.assertEquals("testSolution", userTest.getSolution());
        Assert.assertEquals("testEmail", userTest.getEmail());
        Assert.assertEquals("testPassword", userTest.getPassword());
    }

}
