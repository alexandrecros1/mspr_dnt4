package fr.epsi.Controller;

import fr.epsi.beans.Fcsv;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import fr.epsi.bdd.Inscription;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImportServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    public static final int TAILLE_TAMPON = 10240;
    public static final String CHEMIN_FICHIERS = "C:\\test\\";

    public ImportServlet() {
        super();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/import.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ServletException | IOException e) {
            e.getMessage();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // On récupère le champ description comme d'habitude
        String description = request.getParameter("description");
        request.setAttribute("description", description);

        try {
            // On récupère le champ du fichier
            Part part = request.getPart("fichier");
            // On vérifie qu'on a bien reçu un fichier
            String nomFichier = getNomFichier(part);

            // Si on a bien un fichier
            if (nomFichier != null && !nomFichier.isEmpty()) {
                String nomChamp = part.getName();
                // Corrige un bug du fonctionnement d'Internet Explorer
                nomFichier = nomFichier.substring(nomFichier.lastIndexOf('/') + 1)
                        .substring(nomFichier.lastIndexOf('\\') + 1);

                // On écrit définitivement le fichier sur le disque
                ecrireFichier(part, nomFichier, CHEMIN_FICHIERS);
                request.setAttribute(nomChamp, nomFichier);
                lire(nomFichier);

            }
            processRequest(request, response);
            
        } catch (ClassNotFoundException | CsvValidationException | ServletException | IOException | SQLException e) {
            Logger.getLogger(ImportServlet.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void ecrireFichier(Part part, String nomFichier, String chemin) throws IOException {
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;

        entree = new BufferedInputStream(part.getInputStream(), TAILLE_TAMPON);
        sortie = new BufferedOutputStream(new FileOutputStream(new File(chemin + nomFichier)), TAILLE_TAMPON);
        try {

            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur;
            while ((longueur = entree.read(tampon)) > 0) {
                sortie.write(tampon, 0, longueur);
            }
        } catch (IOException e) {
            e.getMessage();
        } finally {
            sortie.close();
        }

    }

    protected void lire(String nomFichier) throws IOException, CsvValidationException, ClassNotFoundException, SQLException {
        File file = new File(CHEMIN_FICHIERS + nomFichier);
        FileReader fr = new FileReader(file);
        try (CSVReader csvReader = new CSVReader(fr)) {

            List<String[]> data;
            data = new ArrayList<>();
            String[] nextLine = null;
            while ((nextLine = csvReader.readNext()) != null) {
                int size = nextLine.length;

                String debut = nextLine[0].trim();
                if (debut.length() == 0 && size == 1 && debut.startsWith("#")) {
                    continue;
                }

                data.add(nextLine);
                String regex1 = "\\;.*$";
                String regex2 = "^[^;]*;";
                String libelle = nextLine[0].replaceAll(regex1, "");
                String valeur = nextLine[0].replaceAll(regex2, "");

                Fcsv fi = new Fcsv();
                fi.setLibelle(libelle);
                fi.setValeur(valeur);
                Inscription tablecsv = new Inscription();
                tablecsv.addcsv(fi);
            }
        }
    }

    private static String getNomFichier(Part part) {
        for (String contentDisposition : part.getHeader("content-disposition").split(";")) {
            if (contentDisposition.trim().startsWith("filename")) {
                return contentDisposition.substring(contentDisposition.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
