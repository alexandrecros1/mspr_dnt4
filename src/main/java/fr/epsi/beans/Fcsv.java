package fr.epsi.beans;

public class Fcsv {

    private int idFiche;
    private String libelle;
    private String valeur;

    public Fcsv() {
    }

    public Fcsv(int idFiche, String libelle, String valeur) {
        this.idFiche = idFiche;
        this.libelle = libelle;
        this.valeur = valeur;
    }

    public int getIdFiche() {
        return idFiche;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getValeur() {
        return valeur;
    }

    public void setIdFiche(int idFiche) {
        this.idFiche = idFiche;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

}
