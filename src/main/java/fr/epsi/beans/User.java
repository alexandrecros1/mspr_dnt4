package fr.epsi.beans;

public class User {

    private int idUser;
    private String nom;
    private String prenom;
    private String solution;
    private String email;
    private String password;

    public User() {
    }

    public User(int idUser, String nom, String prenom, String solution, String email, String password) {
        this.idUser = idUser;
        this.nom = nom;
        this.prenom = prenom;
        this.solution = solution;
        this.email = email;
        this.password = password;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getSolution() {
        return solution;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
