package fr.epsi.bdd;

import fr.epsi.beans.Fcsv;
import fr.epsi.beans.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Inscription {

    private Connection connexion;

    public boolean conex(String email, String password) throws SQLException, ClassNotFoundException {
        List<User> sac = new ArrayList<>();
        connexion = null;
        ResultSet resultat = null;
        loadDatabase();
        try (Statement statement = connexion.createStatement()) {

            resultat = statement.executeQuery("SELECT email,password FROM user;");

            while (resultat.next()) {
                String emailU = resultat.getString("email");
                String passwordU = resultat.getString("password");

                User user = new User();
                user.setEmail(emailU);
                user.setPassword(passwordU);
                sac.add(user);

            }

            connexion.close();

            for (int i = 0; i < sac.size(); i++) {
                if (sac.get(i).getEmail().equals(email) && sac.get(i).getPassword().equals(password)) {
                    return true;
                }
            }

        } catch (SQLException e) {
            e.getMessage();
        }
        return false;
    }

    public void addcsv(Fcsv fcsv) throws ClassNotFoundException, SQLException {
        loadDatabase();

        try (PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO fiche(idFiche,libelle,valeur)"
                + "VALUES (?, ?, ?);")) {
            preparedStatement.setInt(1, fcsv.getIdFiche());
            preparedStatement.setString(2, fcsv.getLibelle());
            preparedStatement.setString(3, fcsv.getValeur());

            preparedStatement.executeUpdate();
        }

    }

    public void addUser(User user) throws ClassNotFoundException, SQLException {
        loadDatabase();
        try (PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO user(idUser,nom,prenom,solution,email,password)"
                + "VALUES (?, ?, ?, ?, ?, ?);")) {
            preparedStatement.setInt(1, user.getIdUser());
            preparedStatement.setString(2, user.getNom());
            preparedStatement.setString(3, user.getPrenom());
            preparedStatement.setString(4, user.getSolution());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getPassword());

            preparedStatement.executeUpdate();
        }
    }

    private void loadDatabase() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        String password = System.getProperty("database.password");
        connexion = DriverManager.getConnection("jdbc:mysql://mysql2.montpellier.epsi.fr:5306/opendata", "alexandre.cros1", "rootroot");
    }

}
