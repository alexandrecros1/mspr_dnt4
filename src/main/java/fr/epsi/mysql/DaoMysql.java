package fr.epsi.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DaoMysql extends Config {

    private Connection cnx;
    private PreparedStatement pstm;
    private int ok;
    private ResultSet rs;

    public void getConnection() {
        cnx = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String password = System.getProperty("database.password");
            cnx = DriverManager.getConnection("jdbc:mysql://" + Config.DBHOST + ":" + Config.DBPORT + "/" + Config.DBNAME,
                    Config.DBUSER, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.getMessage();
        }

    }

    public void initPS(String sql) {
        getConnection();
        try {
            if (sql.toLowerCase().startsWith("insert")) {
                pstm = cnx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                pstm = cnx.prepareStatement(sql);
            }
        } catch (SQLException e) {
            e.getMessage();
        }

    }

    public int executeMaj() {
        try {
            ok = pstm.executeUpdate();
        } catch (SQLException e) {
            e.getMessage();
        }
        return ok;
    }

    public ResultSet executeSelect() {
        try {
            rs = pstm.executeQuery();
        } catch (SQLException e) {
            e.getMessage();
        }
        return rs;
    }

    public PreparedStatement getPstm() {
        return this.pstm;

    }

    public void closeConnection() {
        try {
            if (cnx != null) {
                cnx.close();
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    public static DaoMysql getInstance() {
        return new DaoMysql();
    }

}
