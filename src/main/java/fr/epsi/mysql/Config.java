package fr.epsi.mysql;

public class Config {
    
    protected static final String DBHOST = "localhost";
    protected static final String DBPORT = "3306";
    protected static final String DBUSER = "root";
    protected static final String DBNAME = "opendata";
}
