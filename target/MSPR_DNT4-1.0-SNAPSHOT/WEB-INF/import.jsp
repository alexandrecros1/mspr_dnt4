<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


    <body>
        <img src="asset/BandeauConnecte.png" style="width: 100%;"></img>
        <c:if test="${ !empty fichier }"><p ><c:out value="Le fichier ${ fichier } (${ description }) a été uploadé !" /></p></c:if>
        <p style="text-align: center; margin-top: 100px;margin-bottom: -10%">Zone de dépôt</p>
        <form method="post" action="ImportServlet" enctype="multipart/form-data">
            <div class="form-group">
                <label for="fichier">Fichier à envoyer : </label>
                <input type="file"  name="fichier" id="fichier"  />

                <input type="submit" style="background-color: rgb(226, 226, 226) ; color: black; border-radius: 25px ; border: black; margin-left:28%; width: 200px; height:40px"/>
            </div>
        </form>
        <p style="text-align: center;">
            <img src="asset/infoUpload.png" style="width: 20%;"></img>
        </p>
        
    </body>
</html>
<style>

    form {
        margin: 0 auto;
        width: 500px;
        padding: 2em;
        border: 1px solid #b0416c;
        border-radius: 1em;
        margin: 200px auto 0px auto; 
    }

</style> 